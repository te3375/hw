const headers = document.querySelectorAll('.card__head__description--basic h3'),
      descriptions = document.querySelectorAll('.description__text--basic');

headers.forEach(header => {
  header.innerText = header.innerText.toUpperCase();
});

descriptions.forEach(description => {
  const textSize = description.innerText.length;
  if(textSize > 20) {
    description.innerText = description.innerText.slice(0,20) + '...';
  }
});


// --------------↓ Homework 7

const navList = document.querySelector('.navigation__lists');

navList.addEventListener('click', handleNavigation);

function handleNavigation(event) {
  const target = event.target;
  
  if(!target.classList.contains('item__label--with-arrow')) {
    return;
  }

  const itemContent = target.nextElementSibling,
        elemHide = itemContent.classList.contains('item__content--hidden'),
        img = target.firstElementChild;
        itemContent.classList.toggle('item__content--hidden');

  if(elemHide) {
    img.classList.remove('remove_rotate_img');
    img.classList.add('rotate_img');
  } else {
    img.classList.remove('rotate_img');
    img.classList.add('remove_rotate_img');
  }
}

// --------------↑ Homework 7

// --------------↓ Homework 9

function createCourseObject() {

  const groups = document.querySelectorAll('[data-group]');
  const obj = {};

  // удаляем дубликаты 

  const arr = [...new Set(groups)];

  // в каждый элемент группы вкладываем массив с опциями
  for(elem of arr) {
    const item = elem.getAttribute('data-group');
    const cards = document.querySelectorAll(`[data-group=${item}]`);
    const optionsArr = [];
    cards.forEach(card => {
      const description = card.querySelector('.description__text').innerText;
      const date = card.querySelector('.navigate__date').innerText;
      const img = card.querySelector('img').src;
      const label = card.querySelector('.card__head__type').innerText;

      optionsArr.push({
        "title": item, 
        "description": description, 
        "date": date, 
        "image": img, 
        "label": label,
      })
    })
    obj[item] = optionsArr;
  }

  return console.log(obj);

}

createCourseObject();
// --------------↑ Homework 9